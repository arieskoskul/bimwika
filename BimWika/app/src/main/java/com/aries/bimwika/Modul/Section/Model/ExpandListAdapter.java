package com.aries.bimwika.Modul.Section.Model;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.TextView;

import com.aries.bimwika.Modul.Master.Model.File;
import com.aries.bimwika.Modul.Master.Model.Section;
import com.aries.bimwika.R;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by zxy on 17/4/23.
 */

public class ExpandListAdapter extends BaseExpandableListAdapter {
//menwariskan class BaseExpandableListAdapter



    private Context context;
    private List<Section> sections;

    //deklarasi var context dan groups


    public ExpandListAdapter(Context context, List<Section> sections) {
        this.context = context;
        this.sections = sections;
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {


        List<File> file = sections.get(groupPosition).getList_fie();

        return file.get(childPosition);

        //mengambil nilai atau value dari child
    }



    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
        //mengambil id dari child
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild,
                             View convertView, ViewGroup parent) {

        File file = (File) getChild(groupPosition,childPosition);
        if(convertView == null){
            LayoutInflater layoutInflater = (LayoutInflater)context
                    .getSystemService(context.LAYOUT_INFLATER_SERVICE);
            convertView = layoutInflater.inflate(R.layout.activity_file_list,null);

        }

        TextView textView = (TextView)convertView.findViewById(R.id.node_name_view);

        String text = file.getFile_nama();
        textView.setText(text);

        //menampilkan data ke view layout child_item.xml
        return convertView;
    }

    @Override
    public int getGroupCount() {
        return sections.size();
        //mengambil nilai atau value dari jumlah grup
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        List<File> file ;
        file = sections.get(groupPosition).getList_fie();
        //Mengambil jumlah child berdasarkan grup tertentu
        return file.size();
    }

    @Override
    public Object getGroup(int groupPosition) {

        // Mengambil data yang terkait object grup
        return sections.get(groupPosition);
    }

    @Override
    public long getGroupId(int groupPosition) {
//Mengambil id yang terkait grup
        return groupPosition;
    }

    @Override
    public boolean hasStableIds() {
        return true;
        //Menunjukan apakah id dari grup dan child stabil terkait perubahan data di dalamnya
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {

        Section section = (Section) getGroup(groupPosition);

        if(convertView == null){
            LayoutInflater layoutInflater = (LayoutInflater)
                    context.getSystemService(context.LAYOUT_INFLATER_SERVICE);

            convertView = layoutInflater.inflate(R.layout.activity_section_list,null);

            TextView textView = (TextView)convertView.findViewById(R.id.nama_section);
            String text = section.getNama_section();

            textView.setText(text);
        }


        // //menampilkan data ke view layout grup_item.xml
        return convertView;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
        //Menunjukan apakah posisi child tertentu dapat di selectable
    }
}
