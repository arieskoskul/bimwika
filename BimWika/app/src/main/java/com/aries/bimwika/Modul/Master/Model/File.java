package com.aries.bimwika.Modul.Master.Model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class File implements Serializable {
    @SerializedName("id")
    private int id;
    @SerializedName("section_id")
    private int section_id;
    @SerializedName("file_nama")
    private String file_nama;
    @SerializedName("file_url")
    private String file_url;
    @SerializedName("file_tipe")
    private String file_tipe;


    public File(int id, int section_id, String file_nama, String file_url, String file_tipe) {
        this.id = id;
        this.section_id = section_id;
        this.file_nama = file_nama;
        this.file_url = file_url;
        this.file_tipe = file_tipe;
    }

    public String getFile_tipe() {
        return file_tipe;
    }

    public void setFile_tipe(String file_tipe) {
        this.file_tipe = file_tipe;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getSection_id() {
        return section_id;
    }

    public void setSection_id(int section_id) {
        this.section_id = section_id;
    }

    public String getFile_nama() {
        return file_nama;
    }

    public void setFile_nama(String file_nama) {
        this.file_nama = file_nama;
    }

    public String getFile_url() {
        return file_url;
    }

    public void setFile_url(String file_url) {
        this.file_url = file_url;
    }


}
