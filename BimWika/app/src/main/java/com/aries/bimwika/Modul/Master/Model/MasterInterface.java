package com.aries.bimwika.Modul.Master.Model;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;

public interface MasterInterface {
    @POST("masters")
    Call<MasterGet> getMaster();
}
