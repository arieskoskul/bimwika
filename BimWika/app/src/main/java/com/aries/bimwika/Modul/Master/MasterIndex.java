package com.aries.bimwika.Modul.Master;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.aries.bimwika.Modul.Master.Model.Master;
import com.aries.bimwika.Modul.Master.Model.MasterAdapter;
import com.aries.bimwika.Modul.Master.Model.MasterGet;
import com.aries.bimwika.Modul.Master.Model.MasterInterface;
import com.aries.bimwika.Modul.Section.SectionIndex;
import com.aries.bimwika.R;
import com.aries.bimwika.Tools.ApiClient;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MasterIndex extends AppCompatActivity {

    private ListView lv_master_index;
    MasterAdapter master_adapter;
    MasterInterface master_interface;
    List<Master> master_list;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_master_index);
        inisialisasi();
        getData();

        lv_master_index.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapter, View v, int position, long id) {

                Master master = (Master) master_list.get(position);
                Intent i = new Intent(MasterIndex.this, SectionIndex.class);
                i.putExtra("master", master);
                startActivity(i);
            }
        });
    }

    public void inisialisasi(){
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Daftar Master");
        lv_master_index = (ListView) findViewById(R.id.lv_master_index);
        master_interface = ApiClient.getClient().create(MasterInterface.class);
    }



    public void getData() {
        Call<MasterGet> master_call = master_interface.getMaster();
        master_call.enqueue(new Callback<MasterGet>() {
            @Override
            public void onResponse(Call<MasterGet> call, Response<MasterGet>
                    response) {
                master_list = response.body().getList_master();
                Log.d("Retrofit Get", "Jumlah data master: " +
                        String.valueOf(master_list.size()));
                master_adapter = new MasterAdapter(MasterIndex.this, master_list);
                lv_master_index.setAdapter(master_adapter);

            }

            @Override
            public void onFailure(Call<MasterGet> call, Throwable t) {
                Log.e("Retrofit Get: ", t.toString());
            }
        });
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public void onBackPressed() {
        finish();
        Intent i = new Intent(MasterIndex.this, SectionIndex.class);
        startActivity(i);
    }


}
