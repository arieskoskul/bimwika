package com.aries.bimwika.Tools.Pdf;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Toast;

import com.aries.bimwika.Modul.Master.MasterIndex;
import com.aries.bimwika.Modul.Master.Model.MasterAdapter;
import com.aries.bimwika.Modul.Master.Model.MasterGet;
import com.aries.bimwika.R;
import com.aries.bimwika.Tools.Tigadimensi.ModelActivity;
import com.github.barteksc.pdfviewer.PDFView;
import com.github.barteksc.pdfviewer.util.FitPolicy;

import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

import okhttp3.OkHttpClient;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class PdfView extends AppCompatActivity {
    PDFView pdfView;
    String file_url;
    private Map<String, Object> loadModelParameters = new HashMap<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pdf);
        pdfView = findViewById(R.id.pdfView);
        file_url = getIntent().getStringExtra("file_url");

        getData();
    }

    public void getData() {
        PdfLoadRetrofitInterface downloadService = createService(PdfLoadRetrofitInterface.class, "http://192.168.43.77/administrator_bimx/public/api/");
        Call<ResponseBody> call = downloadService.downloadPDF("download/file/"+ file_url);

        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, final Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    displayPDF(response.body().byteStream());
                } else {
                    Log.d("ERROR", "Connection failed " + response.errorBody());
                   ;
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                t.printStackTrace();
                Log.e("ERROR", t.getMessage());
            }
        });
    }

    public <T> T createService(Class<T> serviceClass, String baseUrl) {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(baseUrl)
                .client(new OkHttpClient.Builder().build())
                .build();
        return retrofit.create(serviceClass);
    }

    private void displayPDF(InputStream stream) {
        pdfView.fromStream(stream)
                .enableSwipe(true) // allows to block changing pages using swipe
                .swipeHorizontal(true)
                .enableDoubletap(true)
                .defaultPage(0)
                .enableAnnotationRendering(false) // render annotations (such as comments, colors or forms)
                .password(null)
                .scrollHandle(null)
                .enableAntialiasing(true) // improve rendering a little bit on low-res screens
                // spacing between pages in dp. To define spacing color, set view background
                .spacing(0)
                .pageFitPolicy(FitPolicy.WIDTH)
                .load();
        }

    }
