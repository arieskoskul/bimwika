package com.aries.bimwika.Modul.Master.Model;

import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class Master implements Serializable {
    @SerializedName("id")
    private int id;
    @SerializedName("nama_master")
    private String nama_master;
    @SerializedName("tanggal")
    private String tanggal;
    @SerializedName("section")
    private List<Section> list_section;

    public Master(int id, String nama_master, List<Section> list_section, String tanggal) {
        this.id = id;
        this.nama_master = nama_master;
        this.list_section = list_section;
        this.tanggal = tanggal;
    }


    public String getTanggal() {
        return tanggal;
    }

    public void setTanggal(String tanggal) {
        this.tanggal = tanggal;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNama_master() {
        return nama_master;
    }

    public void setNama_master(String nama_master) {
        this.nama_master = nama_master;
    }

    public List<Section> getList_section() {
        return list_section;
    }

    public void setList_section(List<Section> list_section) {
        this.list_section = list_section;
    }
}
