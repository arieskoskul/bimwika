package com.aries.bimwika.Modul.Master.Model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class MasterGet {
    @SerializedName("masters")
    List<Master> list_master;
    @SerializedName("error")
    Boolean error;

    public MasterGet(List<Master> list_master, Boolean error) {
        this.list_master = list_master;
        this.error = error;
    }

    public List<Master> getList_master() {
        return list_master;
    }

    public void setList_master(List<Master> list_master) {
        this.list_master = list_master;
    }

    public Boolean getError() {
        return error;
    }

    public void setError(Boolean error) {
        this.error = error;
    }
}
