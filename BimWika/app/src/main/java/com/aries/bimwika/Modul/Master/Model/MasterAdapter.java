package com.aries.bimwika.Modul.Master.Model;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.aries.bimwika.R;

import java.util.List;

public class MasterAdapter extends BaseAdapter {
    private Activity activity;
    private List<Master> data_master;
    private static LayoutInflater inflater=null;

    public MasterAdapter(Activity a, List<Master> data_master) {
        activity = a;
        this.data_master = data_master;
        inflater = (LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }

    @Override
    public int getCount() {
        return data_master.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View vi=convertView;
        if(convertView==null)

            vi = inflater.inflate(R.layout.activity_master_index_list, null);

        TextView tv_master_id = (TextView)vi.findViewById(R.id.tv_master_id);
        TextView tv_tanggal_buat = (TextView)vi.findViewById(R.id.tv_tanggal_buat);
        TextView tv_master_nama = (TextView)vi.findViewById(R.id.tv_master_nama);


        Master master = data_master.get(position);

        tv_master_id.setText(String.valueOf(master.getId()));
        tv_tanggal_buat.setText(master.getTanggal());
        tv_master_nama.setText(master.getNama_master());

        return vi;
    }



}
