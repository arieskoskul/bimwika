package com.aries.bimwika.Modul.Master.Model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class Section implements Serializable {
    @SerializedName("id")
    private int id;
    @SerializedName("master_id")
    private int master_id;
    @SerializedName("section_nama")
    private String nama_section;
    @SerializedName("file")
    private List<File> list_fie;

    public Section(int id, int master_id, String nama_section, List<File> list_fie) {
        this.id = id;
        this.master_id = master_id;
        this.nama_section = nama_section;
        this.list_fie = list_fie;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getMaster_id() {
        return master_id;
    }

    public void setMaster_id(int master_id) {
        this.master_id = master_id;
    }

    public String getNama_section() {
        return nama_section;
    }

    public void setNama_section(String nama_section) {
        this.nama_section = nama_section;
    }

    public List<File> getList_fie() {
        return list_fie;
    }

    public void setList_fie(List<File> list_fie) {
        this.list_fie = list_fie;
    }
}
