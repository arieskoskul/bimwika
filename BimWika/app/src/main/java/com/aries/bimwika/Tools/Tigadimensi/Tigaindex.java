package com.aries.bimwika.Tools.Tigadimensi;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;

import com.aries.bimwika.R;

import org.andresoviedo.util.android.AndroidURLStreamHandlerFactory;

import java.net.URL;

public class Tigaindex extends Activity {


    // Custom handler: org/andresoviedo/app/util/url/android/Handler.class
    static {
        System.setProperty("java.protocol.handler.pkgs", "org.andresoviedo.util.android");
        URL.setURLStreamHandlerFactory(new AndroidURLStreamHandlerFactory());
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Set main layout controls.
        // Basically, this is a screen with the app name just in the middle of the scree
        setContentView(R.layout.activity_tiga);

        // Start Model activity.
        Tigaindex.this.startActivity(new Intent(Tigaindex.this.getApplicationContext(), MenuActivity.class));
        Tigaindex.this.finish();
    }

    @SuppressWarnings("unused")
    private void init() {
        Tigaindex.this.startActivity(new Intent(Tigaindex.this.getApplicationContext(), ModelActivity.class));
        Tigaindex.this.finish();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.button, menu);
        return true;
    }
}

