package com.aries.bimwika.Modul.Section;

import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.aries.bimwika.Modul.Master.MasterIndex;
import com.aries.bimwika.Modul.Master.Model.File;
import com.aries.bimwika.Modul.Master.Model.Master;
import com.aries.bimwika.Modul.Master.Model.Section;
import com.aries.bimwika.Modul.Section.Model.ExpandListAdapter;
import com.aries.bimwika.R;
import com.aries.bimwika.Tools.Pdf.PdfView;
import com.aries.bimwika.Tools.Tigadimensi.ModelActivity;

import java.util.List;

import me.texy.treeview.TreeNode;
import me.texy.treeview.TreeView;

public class SectionIndex extends AppCompatActivity {
    private ExpandableListAdapter expandableListAdapter;
    private List<Section> sectionExpandList;
    private ExpandableListView expandableListView;
    protected Toolbar toolbar;
    private Master master;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        super.setContentView(R.layout.activity_main_tree);

        //ambil MASTER
        master = (Master)getIntent().getSerializableExtra("master");

        inisialisasi();

        sectionExpandList = master.getList_section();
        //memberikan nilai pada object groupExpandList berdasarkan method inputData()

        expandableListAdapter = new ExpandListAdapter(this,sectionExpandList);
        //menginstansiasi object dari class Adapter ExpandListAdapter

        expandableListView.setAdapter(expandableListAdapter);

        expandableListView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
            @Override
            public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {
                List<File> file = sectionExpandList.get(groupPosition).getList_fie();

                String file_nama = file.get(childPosition).getFile_nama();
                String file_url = file.get(childPosition).getFile_url();
                String file_tipe = file.get(childPosition).getFile_tipe();

                if(file_tipe.equals("obj")){
                    view3D("http://192.168.43.77/administrator_bimx/public/api/download/file/" + file_url);
                } else if(file_tipe.equals("pdf")){
                    viewPDF(file_nama, file_url);
                }


                //method dari object  expandableListView yang dimana ketika di klik akan menampilkan
                //data dari Child yang ada di dalam Grup
                //serta menampilkan pesan text Toast

                return false;

            }

        });
    }

    public void inisialisasi(){
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Section");
        expandableListView = (ExpandableListView) findViewById(R.id.exp_section_file);
    }

    public void viewPDF(String nama, String url){
    //method untuk menampilkan pesan text toast
        Toast.makeText(this, nama + ", URL: "+ url, Toast.LENGTH_SHORT).show();
        Intent i = new Intent(SectionIndex.this, PdfView.class);
        i.putExtra("file_url", url);
        startActivity(i);
    }

    public void view3D(String url){
        //method untuk menampilkan pesan text toast
        Intent i = new Intent(SectionIndex.this, ModelActivity.class);
        i.putExtra("uri", url);
        startActivity(i);
    }

    /*
    private void buildTree() {
        TreeNode[] tn_level1 = new TreeNode[master.getList_section().size()];
        for(int k=0;k<master.getList_section().size();k++) {
            Log.d("Retrofit Get", "Nama Section: " + master.getList_section().get(k).getNama_section());
            tn_level1[k] = new TreeNode(master.getList_section().get(k).getNama_section());
            tn_level1[k].setLevel(0);


            TreeNode[] tn_level2 = new TreeNode[master.getList_section().get(k).getList_fie().size()];
            for(int j=0;j<master.getList_section().get(k).getList_fie().size();j++){
                Log.e("Retrofit Get File", "Nama File: " + master.getList_section().get(k).getList_fie().get(j).getFile_nama());
                File file = (File) master.getList_section().get(k).getList_fie().get(j);
                tn_level2[j] = new TreeNode(file);
                tn_level2[j].setLevel(1);
                tn_level1[k].addChild(tn_level2[j]);
            }
        }

        root.addChild(tn_level1[0]);
    }*/



}
